student@administrator:~$ git clone https://gitlab.com/fosslab2018/8.2.shellscripting.git
Cloning into '8.2.shellscripting'...
remote: Counting objects: 3, done.
remote: Total 3 (delta 0), reused 0 (delta 0)
Unpacking objects: 100% (3/3), done.
Checking connectivity... done.
student@administrator:~$ cd 8.2.shellscripting
student@administrator:~/8.2.shellscripting$ touch README.md
student@administrator:~/8.2.shellscripting$ git add README.md
student@administrator:~/8.2.shellscripting$ git commit -m "add README"
On branch master
Your branch is up-to-date with 'origin/master'.
nothing to commit, working directory clean
student@administrator:~/8.2.shellscripting$ git push -u origin master
Username for 'https://gitlab.com': ASI16CS008
Password for 'https://ASI16CS008@gitlab.com': 
Branch master set up to track remote branch master from origin.
Everything up-to-date
student@administrator:~/8.2.shellscripting$ git add shell.odt
student@administrator:~/8.2.shellscripting$ git commit -m "add shell"
[master 109ddf2] add shell
 1 file changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 shell.odt
student@administrator:~/8.2.shellscripting$ git push -u origin master
Username for 'https://gitlab.com': ASI16CS008
Password for 'https://ASI16CS008@gitlab.com': 
Counting objects: 3, done.
Delta compression using up to 4 threads.
Compressing objects: 100% (3/3), done.
Writing objects: 100% (3/3), 1.70 MiB | 663.00 KiB/s, done.
Total 3 (delta 0), reused 0 (delta 0)
To https://gitlab.com/fosslab2018/8.2.shellscripting.git
   a64d94d..109ddf2  master -> master
Branch master set up to track remote branch master from origin.